package net.gcvs.deck;

import java.util.Arrays;
import java.util.Objects;

/**
 * a mutable stack.
 * 
 * All methods operate on the stack in-place.
 * 
 * @author gary
 *
 */
public class Stack {
	private Card[] cards;
	private int length;

	public Stack() {
		this.cards = new Card[0];
	}

	/**
	 * array is cloned
	 * 
	 * @param cards
	 */
	public Stack(final Card[] cards) {
		this(cards, true);
	}

	/**
	 * you must know what you're doing if you're not cloning the array.
	 */
	protected Stack(final Card[] cards, final boolean clone) {
		this.cards = clone ? cards.clone() : cards;
		this.length = cards.length;
	}

	public String toString() {
		boolean sep = false;
		final StringBuilder sb = new StringBuilder();
		for (int c = 0, len = this.length; c < len; c++) {
			final Card card = cards[c];
			if (sep) {
				sb.append(' ');
			}
			sb.append(card);
			sep = true;
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(cards);
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof Stack) {
			final Stack s = (Stack) o;
			final int len = size();
			if (s.size() != len) {
				return false;
			}
			for (int i = 1; i <= len; i++) {
				if (!cardAt(i).equals(s.cardAt(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * returns the Card at the given index, 0 based indexing.
	 * 
	 * @param idx
	 * @return
	 */
	public Card get(final int idx) {
		final int index = Objects.checkIndex(idx, length);
		return cards[index];
	}

	/**
	 * returns the Card at the given position. 1 based indexing. position is equivalent to the "stack number" of a card
	 * 
	 * @param position
	 * @return
	 */
	public Card cardAt(final int position) {
		final int index = Objects.checkIndex(position - 1, length);
		return cards[index];
	}

	public int size() {
		return length;
	}

	public Stack clone() {
		// pack cards into array of new length
		final Card[] cards = new Card[length];
		System.arraycopy(this.cards, 0, cards, 0, length);
		return new Stack(cards, false);
	}

	// **************************************** mutable methods below **************************************** //

	public Stack inFaro() {
		final Card[] list = new Card[cards.length];
		int to = 1;
		int from;
		final int len = list.length;
		final int top = len / 2;
		for (from = 0; from < top; from++) {
			list[to] = cards[from];
			to += 2;
		}
		to = 0;
		for (; from < len; from++) {
			list[to] = cards[from];
			to += 2;
		}

		this.cards = list;
		return this;
	}

	public Stack outFaro() {
		final Card[] list = new Card[cards.length];
		int to = 0;
		int from;
		final int len = list.length;
		final int top = (len + 1) / 2;
		for (from = 0; from < top; from++) {
			list[to] = cards[from];
			to += 2;
		}
		to = 1;
		for (; from < len; from++) {
			list[to] = cards[from];
			to += 2;
		}

		this.cards = list;
		return this;
	}

	public Stack cut(final int amount) {
		if (amount < 0 || amount > size()) {
			throw new RuntimeException();
		}
		final Card[] cards = new Card[amount];
		System.arraycopy(this.cards, 0, cards, 0, amount);
		burn(amount);
		return new Stack(cards, false);
	}

	/**
	 * this stack is in-faroed with the given stack
	 * 
	 * @param stack
	 * @return
	 */
	public Stack inFaro(final Stack stack) {
		final int length = size() + stack.size();
		final Card[] list = new Card[length];
		int aidx = 0, alen = size();
		int bidx = 0, blen = stack.size();
		for (int idx = 0; idx < length;) {
			if (aidx < alen) {
				list[idx++] = this.cards[aidx++];
			} else {
				// finished a
				System.arraycopy(stack.cards, bidx, list, idx, length - idx);
				break;
			}
			if (bidx < blen) {
				list[idx++] = stack.cards[bidx++];
			} else {
				// finished b
				System.arraycopy(this.cards, aidx, list, idx, length - idx);
				break;
			}
		}

		this.cards = list;
		this.length = list.length;
		return this;
	}

	/**
	 * this stack is out-faroed with the given stack
	 * 
	 * @param stack
	 * @return
	 */
	public Stack outFaro(final Stack stack) {
		final int length = size() + stack.size();
		final Card[] list = new Card[length];
		int aidx = 0, alen = size();
		int bidx = 0, blen = stack.size();
		for (int idx = 0; idx < length;) {
			if (bidx < blen) {
				list[idx++] = stack.cards[bidx++];
			} else {
				// finished b
				System.arraycopy(this.cards, aidx, list, idx, length - idx);
				break;
			}
			if (aidx < alen) {
				list[idx++] = this.cards[aidx++];
			} else {
				// finished a
				System.arraycopy(stack.cards, bidx, list, idx, length - idx);
				break;
			}
		}

		this.cards = list;
		this.length = list.length;
		return this;
	}

	public Stack onTop(final Stack stack) {
		final int length = size() + stack.size();
		final Card[] list = new Card[length];
		System.arraycopy(stack.cards, 0, list, 0, stack.size());
		System.arraycopy(this.cards, 0, list, stack.size(), size());
		this.cards = list;
		this.length = length;
		return this;
	}

	public Stack onBottom(final Stack stack) {
		final int length = size() + stack.size();
		final Card[] list = new Card[length];
		System.arraycopy(this.cards, 0, list, 0, size());
		System.arraycopy(stack.cards, 0, list, size(), stack.size());
		this.cards = list;
		this.length = length;
		Arrays.fill(stack.cards, 0, stack.length, null);
		stack.length = 0;
		return this;
	}

	public Stack burnAll() {
		Arrays.fill(cards, 0, length, null);
		length = 0;
		return this;
	}

	public Stack burn(final int count) {
		if (count > length || count < 0) {
			throw new RuntimeException();
		}
		final int len = length;
		final int newlen = len - count;
		System.arraycopy(cards, count, cards, 0, newlen);
		length -= count;
		Arrays.fill(cards, newlen, len, null);
		return this;
	}

	public Stack run(final int count) {
		final Card[] list = new Card[count];
		for (int i = 0, idx = count; i < count; i++) {
			list[--idx] = cards[i];
		}

		burn(count);

		return new Stack(list, false);
	}

	public Stack[] deal(final int count, final int stacks) {
		if (count < 0 || count > size()) {
			throw new RuntimeException();
		}
		Stack[] list = new Stack[stacks];
		for (int i = 0; i < stacks; i++) {
			list[i] = new Stack();
		}
		for (int i = 0; i < count; i++) {
			Stack card = cut(1);
			list[i % stacks].onTop(card);
		}
		return list;
	}

	public Stack klondike() {
		final Card[] list = new Card[length];
		int top = 0;
		int bottom = length - 1;
		int idx = bottom;
		while (top < bottom) {
			list[idx--] = cards[bottom--];
			list[idx--] = cards[top++];
		}
		if (top == bottom) {
			list[0] = cards[top];
		}

		this.cards = list;
		return this;
	}
}
