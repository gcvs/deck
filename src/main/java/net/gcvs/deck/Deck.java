package net.gcvs.deck;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for new deck order stacks
 * 
 * @author gary
 *
 */
public class Deck {
	public static final Deck Bicycle = new Deck(
			"AH 2H 3H 4H 5H 6H 7H 8H 9H TH JH QH KH",
			"AC 2C 3C 4C 5C 6C 7C 8C 9C TC JC QC KC",
			"KD QD JD TD 9D 8D 7D 6D 5D 4D 3D 2D AD",
			"KS QS JS TS 9S 8S 7S 6S 5S 4S 3S 2S AS");
	public static final Deck Fournier = new Deck(
			"AS 2S 3S 4S 5S 6S 7S 8S 9S TS JS QS KS",
			"AH 2H 3H 4H 5H 6H 7H 8H 9H TH JH QH KH",
			"KD QD JD TD 9D 8D 7D 6D 5D 4D 3D 2D AD",
			"KC QC JC TC 9C 8C 7C 6C 5C 4C 3C 2C AC");
	public static final Deck SiStebbins = new Deck(
			"6H 9S QD 2C",
			"5H 8S JD AC",
			"4H 7S TD KC",
			"3H 6S 9D QC",
			"2H 5S 8D JC",
			"AH 4S 7D TC",
			"KH 3S 6D 9C",
			"QH 2S 5D 8C",
			"JH AS 4D 7C",
			"TH KS 3D 6C",
			"9H QS 2D 5C",
			"8H JS AD 4C",
			"7H TS KD 3C");
	public static final Deck Mnemonica = new Deck(
			"4C 2H 7D 3C 4H 6D AS 5H 9S 2S QH 3D QC 8H 6S 5S 9H KC 2D JH 3S 8S 6H TC 5D KD 2C 3H 8D 5C KS JD 8C TS KH JC 7S TH AD 4S 7H 4D AC 9C JS QD 7C QS TD 6C AH 9D");
	public static final Deck Aronson = new Deck(
			"JS KC 5C 2H 9S AS 3H 6C 8D AC TS 5H 2D KD 7D 8C 3S AD 7S 5S QD AH 8S 3D 7H QH 5D 7C 4H KH 4D TD JC JH TC JD 4S TH 6H 3C 2S 9H KS 6S 4C 8H 9C QS 6D QC 2C 9D");
	public static final Deck StayStack = new Deck();
	public static final Deck EightKings = new Deck();
	public static final Deck Nikola = new Deck(
			"6D 5C KC JH 5S 9D 9S QH 3C TC KS AH 4D JD KD KH 2D QC 9C TH 8D 2C AC 7H 7C 4S 7S 9H 8S 6S 6C 2H AS JS 4C 5H TS AD JC 4H 2S 7D QS 3H 3S 8C TD 6H 5D 3D QD 8H");

	// Osterlind Breakthrough Card System
	public static final Deck Osterlind = new Deck(
			"AS 3S 7D 5H QC AC 5S JH JC QD 2D 8C 6S KH 2H 6D 3D TS 8D 7C 4S 9D 9C 8H 5D AD 6H AH 4D QS QH KC 3C 9H 7S 2S 5C KD 4H TC TD JS TH 9S 6C 2C 7H 3H 8S 4C JD KS");

	private final Card[] cards;

	public Deck(final String... names) {
		final List<Card> cards = new ArrayList<>();
		for (int i = 0, len = names.length; i < len; i++) {
			String[] cs = names[i].split(" +");
			for (final String c : cs) {
				if (c.length() == 0) {
					continue;
				}
				cards.add(Card.of(c));
			}
		}
		this.cards = cards.toArray(new Card[cards.size()]);
	}

	public Stack stack() {
		return new Stack(cards);
	}

	public Stack stack(final Back back) {
		final Card[] cards = this.cards.clone();
		for (int i = 0, len = cards.length; i < len; i++) {
			final Card c = cards[i];
			cards[i] = new Card(c.rank, c.suit, back);
		}
		return new Stack(cards, false);
	}

	/**
	 * returns the card at the given "stack number"
	 * 
	 * @param stackNumber
	 * @return
	 */
	public Card cardAt(final int stackNumber) {
		return cards[stackNumber - 1];
	}

	public int stackNumber(final Card card) {
		for (int i = 0, len = cards.length; i < len; i++) {
			if (cards[i].equals(card)) {
				return i + 1;
			}
		}
		return -1;
	}

	/**
	 * verifies that each card is different
	 */
	public void validate() {
		final int len = cards.length;
		for (int a = 0; a < len; a++) {
			for (int b = 0; b < len; b++) {
				if (a == b) {
					continue;
				}
				if (cards[a].equals(cards[b])) {
					throw new RuntimeException();
				}
			}
		}
	}
}
