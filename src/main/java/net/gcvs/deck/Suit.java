package net.gcvs.deck;

import java.util.HashMap;
import java.util.Map;

/**
 * A card's suit, immutable.
 * 
 * @author gary
 *
 */
public class Suit {
	private static final Map<String, Suit> intern = new HashMap<>();

	public static final Suit HEARTS = new Suit("H", "Hearts");
	public static final Suit CLUBS = new Suit("C", "Clubs");
	public static final Suit DIAMONDS = new Suit("D", "Diamonds");
	public static final Suit SPADES = new Suit("S", "Spades");

	public static final Suit[] HSDC = { HEARTS, SPADES, DIAMONDS, CLUBS };
	public static final Suit[] HCDS = { HEARTS, CLUBS, DIAMONDS, SPADES };
	public static final Suit[] SHCD = { SPADES, HEARTS, CLUBS, DIAMONDS };

	public final String name;
	public final String longName;

	public Suit(final String name, final String longName) {
		this.name = name;
		this.longName = longName;
		intern.put(name, this);
		intern.put(name.toUpperCase(), this);
		intern.put(name.toLowerCase(), this);
		intern.put(longName, this);
		intern.put(longName.toUpperCase(), this);
		intern.put(longName.toLowerCase(), this);
	}

	public String toString() {
		return name;
	}

	public static Suit of(final String code) {
		return intern.get(code);
	}

	public static Suit of(final char code) {
		return of(Character.toString(code));
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof Suit) {
			final Suit s = (Suit) o;
			return name.equals(s.name);
		}
		return false;
	}
}
