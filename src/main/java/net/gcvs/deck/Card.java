package net.gcvs.deck;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Immutable Card, front and back.
 * 
 * @author gary
 *
 */
public class Card {
	public final Rank rank;
	public final Suit suit;
	public final Back back;

	private String name2;

	public Card(final Rank rank, final Suit suit) {
		this(rank, suit, null);
	}

	public Card(final Rank rank, final Suit suit, final Back back) {
		this.suit = suit;
		this.rank = rank;
		this.back = back;
		this.name2 = "" + rank + suit;
	}

	public String toString() {
		return name2;
	}

	private static final Map<String, Card> intern = new HashMap<>();

	public static Card of(final Rank rank, final Suit suit) {
		final String code = rank.name + suit.name;
		Card card = intern.get(code);
		if (card == null) {
			card = new Card(rank, suit);
			intern.put(code, card);
		}
		return card;
	}

	public static Card of(final String code) {
		final Rank rank = Rank.of(code.charAt(0));
		final Suit suit = Suit.of(code.charAt(1));
		return of(rank, suit);
	}

	@Override
	public int hashCode() {
		int hash = 31 + rank.hashCode();
		hash = 31 * hash + suit.hashCode();
		hash = 31 * hash + (back == null ? 0 : back.hashCode());

		return hash;
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof Card) {
			final Card c = (Card) o;
			return Objects.equals(rank, c.rank)
					&& Objects.equals(suit, c.suit)
					&& Objects.equals(back, c.back);
		}
		return false;
	}
}
