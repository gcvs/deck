package net.gcvs.deck;

/**
 * The back of a card. Object identity equality
 * 
 * @author gary
 *
 */
public class Back {
	public static final Back blue = new Back("blue");
	public static final Back red = new Back("red");

	public final String name;

	public Back(final String name) {
		this.name = name;
	}
}
