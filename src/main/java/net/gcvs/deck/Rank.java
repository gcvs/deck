package net.gcvs.deck;

import java.util.Objects;

/**
 * A card's rank, immutable.
 * 
 * @author gary
 *
 */
public class Rank {
	private static final Rank[] intern = new Rank[256];

	public static final Rank ACE = new Rank("A", "Ace");
	public static final Rank TWO = new Rank("2", "Two");
	public static final Rank THREE = new Rank("3", "Three");
	public static final Rank FOUR = new Rank("4", "Four");
	public static final Rank FIVE = new Rank("5", "Five");
	public static final Rank SIX = new Rank("6", "Six");
	public static final Rank SEVEN = new Rank("7", "Seven");
	public static final Rank EIGHT = new Rank("8", "Eight");
	public static final Rank NINE = new Rank("9", "Nine");
	public static final Rank TEN = new Rank("T", "Ten");
	public static final Rank JACK = new Rank("J", "Jack");
	public static final Rank QUEEN = new Rank("Q", "Queen");
	public static final Rank KING = new Rank("K", "King");

	public static final Rank[] ACE_TO_KING = { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING };
	public static final Rank[] KING_TO_ACE = { KING, QUEEN, JACK, TEN, NINE, EIGHT, SEVEN, SIX, FIVE, FOUR, THREE, TWO, ACE };

	public static final Rank[] ACE_LOW = ACE_TO_KING;
	public static final Rank[] ACE_HIGH = { TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };

	// Eight kings threatened to save, nine fine ladies for one sick knave
	public static final Rank[] EIGHT_KINGS = { EIGHT, KING, THREE, TEN, TWO, SEVEN, NINE, FIVE, QUEEN, FOUR, ACE, SIX, JACK };

	public final String name;
	public final String longName;

	public Rank(final String name, final String longName) {
		this.name = name;
		this.longName = longName;
		intern[name.charAt(0)] = this;
	}

	public static Rank of(final String code) {
		return of(code.charAt(0));
	}

	public static Rank of(final char code) {
		final Rank rank = intern[code];
		Objects.requireNonNull(rank);
		return rank;
	}

	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof Rank) {
			final Rank r = (Rank) o;
			return name.equals(r.name);
		}
		return false;
	}
}
