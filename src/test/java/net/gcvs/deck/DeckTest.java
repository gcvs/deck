package net.gcvs.deck;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class DeckTest {
	@Test
	public void test() {

		final Deck deck = Deck.Bicycle;
		Stack stack = deck.stack();

		System.out.println(stack);
		for (int i = 0; i < 8; i++) {
			stack = stack.outFaro();
			System.out.println(stack);
			boolean match = deck.stack().equals(stack);
			if (match) {
				System.out.println("match after " + (i + 1) + " out faros");
			}
		}

		System.out.println();
		for (int i = 0; i < 52; i++) {
			stack = stack.inFaro();
			System.out.println(stack);
			boolean match = deck.stack().equals(stack);
			if (match) {
				System.out.println("match after " + (i + 1) + " in faros");
			}
		}
	}

	@Test
	public void test2() {

	}

	@Test
	public void verifySiStebbins() {
		Card[] cards = new Card[52];
		Rank[] ranks = Rank.ACE_TO_KING;
		Suit[] suits = Suit.HSDC;
		int rank = 5;
		int suit = 0;
		for (int i = 0; i < 52; i++) {
			cards[i] = Card.of(ranks[rank % ranks.length], suits[suit % suits.length]);
			rank += 3;
			suit++;
		}
		Stack stack = new Stack(cards);
		Assert.assertEquals(Deck.SiStebbins.stack(), stack);
	}

	@Test
	public void verifyOsterlind() {
		Card[] cards = new Card[52];
		Rank[] ranks = Rank.ACE_TO_KING;
		Suit[] suits = Suit.SHCD;
		int rank = 1;
		int suit = 1;
		for (int idx = 0; idx < cards.length; idx++) {
			cards[idx] = Card.of(ranks[rank - 1], suits[suit - 1]);
			System.out.println(cards[idx]);
			// next ranks is double rank, add suit
			int newrank = 1 + (2 * rank + suit - 1) % 13;
			int newsuit;
			switch ((newrank - 1) / 3) {
			case 0:
				// same
				newsuit = 1 + (suit - 1) % 4;
				break;
			case 1:
				// opposite
				newsuit = 1 + (suit - 1 + 2) % 4;
				break;
			case 2:
				// before
				newsuit = 1 + (suit - 1 + 3) % 4;
				break;
			case 3:
			case 4:
				// after
				newsuit = 1 + (suit - 1 + 1) % 4;
				break;
			default:
				throw new RuntimeException();
			}

			rank = newrank;
			suit = newsuit;
		}

		Stack stack = new Stack(cards);
		Assert.assertEquals(Deck.Osterlind.stack(), stack);
	}

	@Test
	public void verifyMnemonicaWithFaros() {
		Stack stack = Deck.Fournier.stack();

		// 4 out faros
		stack.outFaro().outFaro().outFaro().outFaro();
		// reverse the order of the top 26 cards
		stack.onTop(stack.run(26));
		// cut off the top 18 cards and out-faro this stack into the top portion of the stack
		stack.outFaro(stack.cut(18));

		// cut 9 cards off the top and onto the bottom
		stack.onBottom(stack.cut(9));

		Assert.assertEquals(Deck.Mnemonica.stack(), stack);
	}

	@Test
	public void verifyMnemonicaWithoutFaros() {
		Stack stack = Deck.Fournier.stack();
		// reverse the sequence of all 52 cards
		stack.onTop(stack.run(52));

		// do a quadruple anti-faro
		Stack[] stacks = stack.deal(52, 16);

		Stack one = stacks[7].onBottom(stacks[10]).onBottom(stacks[13]).onBottom(stacks[0]);
		Stack two = stacks[11].onBottom(stacks[14]).onBottom(stacks[1]).onBottom(stacks[4]);
		Stack three = stacks[15].onBottom(stacks[2]).onBottom(stacks[5]).onBottom(stacks[8]);
		Stack four = stacks[3].onBottom(stacks[6]).onBottom(stacks[9]).onBottom(stacks[12]);

		stack.onTop(one).onTop(two).onTop(three).onTop(four);

		// split the deck at the center
		Stack top = stack.cut(26);
		// run the top 8 cards onto the lower stack
		stack.onTop(top.run(8));
		// drop the top stack ontop
		stack.onTop(top);

		// knondike shuffle the top 36 cards back ontop ot the stack
		stack.onTop(stack.cut(36).klondike());

		// cut the 9 diamonds to the bottom
		stack.onBottom(stack.cut(9));

		Assert.assertEquals(Deck.Mnemonica.stack(), stack);
	}
}
